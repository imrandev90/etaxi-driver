package ke.co.etaxidriver.interfaces;

public interface DistanceListener {
    void onComplete(String duration, String distance);
}
