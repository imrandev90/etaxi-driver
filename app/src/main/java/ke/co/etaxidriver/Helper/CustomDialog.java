package ke.co.etaxidriver.Helper;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;

import ke.co.etaxidriver.R;

public class CustomDialog extends Dialog {

    public CustomDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);
    }
}
