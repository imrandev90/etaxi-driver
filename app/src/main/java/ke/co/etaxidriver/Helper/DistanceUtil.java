package ke.co.etaxidriver.Helper;

import android.content.Context;
import android.util.Log;


import ke.co.etaxidriver.R;
import ke.co.etaxidriver.Utilities.Utilities;
import ke.co.etaxidriver.interfaces.DistanceListener;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DistanceUtil {
    public static String base = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric";
    public static void getDistance(final Context context, LatLng origin, LatLng destination, final DistanceListener listener) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, DistanceUtil.base+"&origins="+destination.latitude+","+origin.longitude+"&destinations="+destination.latitude+","+destination.longitude+"&key="+context.getString(R.string.google_map_api), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Response", response.toString());

                try {

                    JSONArray rowsArray = response.getJSONArray("rows");
                    JSONArray elementsArray = rowsArray.getJSONObject(0).getJSONArray("elements");
                    JSONObject elementObject = elementsArray.getJSONObject(0);

                    JSONObject distanceObject = elementObject.getJSONObject("distance");
                    JSONObject durationObject = elementObject.getJSONObject("duration");

                    String distance = distanceObject.optString("text");
                    String duration = durationObject.optString("text");

                    listener.onComplete(distance,duration);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String json = "";
                NetworkResponse response = error.networkResponse;

                if (response != null && response.data != null) { }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                return headers;
            }
        };

        XuberApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }
}
