package ke.co.etaxidriver.Draw;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
